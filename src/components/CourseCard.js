import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


// {courseProp} - {} use to deconstruct data
export default function CourseCard({courseProp}) {
	
	// console.log(courseProp);
		// expected result is coursesData[0]
	// console.log(typeof courseProp);
		// result: object

	// // count = 0 due to useState has value of 0
	// const [count, setCount] = useState(0);
	// // syntax: const [getter, setter] = useState(initialValueOfGetter)

	// // Activity 2 
	// const [seat, seatCount] = useState(30);
	// //const [isOpen, setIsOpen] = useState(true);

	// function enroll() {
		
	// 	// if(seat == 0) {
	// 	// 	return alert(`No more seat for ${name}`); 
	// 	// }
	// 	// else{
	// 	// 	setCount(count + 1);
	// 	// 	seatCount(seat - 1);
	// 	// 	console.log('Enrollees' + count)
	// 	// }

	// 		if(seat > 0){
	// 			setCount(count + 1);
	// 			seatCount(seat - 1);
	// 			console.log('Enrollees' + count)
	// 		}
	// };


	// useEffect(() => {
	// 	if (seat === 0){
	// 		alert(`No more seat available`); 
	// 	}
	// }, [seat])
	// syntax: useEffect(() => {}, [optionalParameter])




	const {name, description, price, _id} = courseProp;

	// console.log({name: name});

	// End of Activity 2




// Activity 1

	return (
		<Row>
			<Col>
			{/*<Col xs={12} md={4}>*/}
				<Card className="cardCourse p-3">
					<Card.Body>
						<Card.Title>
							<h5>{name}</h5>
						</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>Php {price}</Card.Text>
						{/*<Card.Text>Enrollees: {count}</Card.Text>
						<Card.Text>Available Seat: {seat}</Card.Text>*/}
						<Button variant="primary" as={Link} to={`/courses/${_id}`}>See Details</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		)
}