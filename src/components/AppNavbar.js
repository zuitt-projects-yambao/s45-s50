import {useState, useContext} from 'react';
import {Navbar, Container, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
//v5: NavLink, Link
import UserContext from '../UserContext';

export default function AppNavbar() {

	// Note this is comming from app.js
	const {user} = useContext(UserContext);

	// const [user, setUser] = useState(localStorage.getItem("email"));

	console.log(user);


	return (
			<Navbar bg="light" expand="lg">
			  <Container fluid className="mx-3">
			    <Navbar.Brand as={Link} to="/">React-Bootstrap</Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
			      <Nav className="ms-auto">
			        <Nav.Link as={Link} to="/">Home</Nav.Link>
			        <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
			        {	(user.id !== null)?
			        	<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
			        	:
			        	<>
				        <Nav.Link as={Link} to="/register">Register</Nav.Link>
				        <Nav.Link as={Link} to="/login">Log in</Nav.Link>
				        </>
			    	}
			      </Nav>
			    </Navbar.Collapse>
			  </Container>
			</Navbar>
		)
}