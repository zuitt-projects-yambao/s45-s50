import {useState, useEffect, useContext} from 'react';
import {Form, Button, Row, Col} from 'react-bootstrap';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login(props) {

	// allows us to consume the User context object and its properties to use for user validation
	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);


	// console.log(email);
	// console.log(password);

	function authenticate(e) {
		//This prevent page from going to default state or it will refresh the whole page
		e.preventDefault();


		// Accessing the backend HTTP user login
		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},

			// Note: email and password came from the useState. This will be data that you got from the FORM.control email and password 
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.access !== "undefined"){

				// data need to be in specific object access to be able to show or else it will appear as [object object]
				localStorage.setItem('token', data.access);

				// type of data/token passing to function retrieveUserDetails
				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			}

			else {
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Please check your credentials!'
				})
			}


		})
		.catch(err => console.log("This is the ERROR : " + err))




		// set the email of the authenticated user in the local storage

		// Syntax: localStorage.setItem("key", value)
		// localStorage.setItem("email", email)

		// to access the user information, it can be done using localStorage, this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout

		// when state change components are rerendered and the AppNavbar component will be updated based on the user credentials

		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		setEmail('');
		setPassword('');


		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details', {
				method: "POST",
				headers: {
					// Authorization from accessing token. The Bearer is needed for selecting a authorization type
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				// retrieve id and isAdmin from the value of data
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}


		// alert(`You are now logged in ${email}`)
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}
		else {
			setIsActive(false);
		}
	}, [email, password])


	return (
		(user.id !== null)?
		<Navigate to="/courses" />
		:
		<Row>
			<Col xs={12} md={4}>
				<Form className="mt-3" onSubmit={e => authenticate(e)}>
					<h1>Login</h1>
					<Form.Group controlId="email">
						<Form.Label>Email:</Form.Label>
						<Form.Control 
							type = "email"
							placeholder = "Email"
							value={email}
							onChange={e => setEmail(e.target.value)}
							required
						/>
					</Form.Group>
					<Form.Group controlId="password">
						<Form.Label>Password:</Form.Label>
						<Form.Control 
							type = "password"
							placeholder = "Password"
							value={password}
							onChange={e => setPassword(e.target.value)}
							required
						/>
					</Form.Group>
					{ isActive ?
						<Button className="my-3" variant="primary" type="submit" id="loginBtn">Login</Button>
						:
						<Button className="my-3" variant="danger" type="submit" id="loginBtn" disabled>Login</Button>
					}
					
				</Form>
			</Col>
		</Row>
	)
}