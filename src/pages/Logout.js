import {Navigate} from 'react-router-dom';
// v5: Redirect to navigate
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';


export default function Logout(){
	
	const {unsetUser, setUser} = useContext(UserContext);

	// clears the localStorage of the user's information
	unsetUser();

	// localStorage.clear();

	useEffect(() => {
		// set the user state back to its original value
		setUser({id: null})
	}, [])
	
	return (
		<Navigate to='/login' />
	)
}