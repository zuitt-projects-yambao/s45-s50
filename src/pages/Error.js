// import {Link} from 'react-router-dom';
// import {Row, Col} from 'react-bootstrap';

import Banner from '../components/Banner';

export default function Error(){

	const data = {
		title: "404: Page Not Found",
		content: "The page you are looking for does not exist",
		destination: "/",
		label: "Back to Homepage"
	}

	return(

		<Banner data={data}/>

	)


	// return (
	// 	<Row className="p-5">
	// 		<Col>
	// 			<h1>Page Not Found</h1>
	// 			<span>Go back to the </span><a href="/" as={Link} to="/">homepage</a>
	// 		</Col>
	// 	</Row>
	// )
}