import { Fragment } from 'react';
// import {  } from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';

export default function Home(){
	
	const data = {

		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/courses",
		label: "Enroll Now!"
	}

	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights/>
			{/*<CourseCard/>*/}
		</Fragment>
	)
	

	// return (

	// 	<Fragment>
	// 		<Banner />
	// 		<Highlights />
	// 		{/*<CourseCard />*/}
	// 	</Fragment>

	// )
}